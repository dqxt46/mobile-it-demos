var page = require('webpage').create();

var loadInProgress;
var testindex = 0;

page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.onLoadStarted = function() {
  loadInProgress = true;
  //console.log("load started");
};

page.onLoadFinished = function() {
  loadInProgress = false;
  //console.log("load finished");
};

var steps = [
  function() {
    //Load Login Page
    page.open("http://partners.motorolasolutions.com");
  },
  function() {
    //Enter Credentials
    page.evaluate(function() {

    	document.getElementById('userid').value = '<insert userid here>';
  		document.getElementById('password').value = '<insert password here>';
 
    });
  }, 
  function() {
    //Login
    page.evaluate(function() {
    	document.getElementById('loginhere').submit();
    });
  },
  function() {
    // Output content of page to stdout after form has been submitted
   page.render("firstPage.png");
  },
  function() {
    page.evaluate(function() {
      	console.log(document.getElementsByClassName('nine mobile-three columns')[0].childNodes[0].childNodes[0].innerHTML);
    });
  }
];

interval = setInterval(function() {
  if (!loadInProgress && typeof steps[testindex] == "function") {
    //console.log("step " + (testindex + 1));
    steps[testindex]();
    testindex++;
  }
  if (typeof steps[testindex] != "function") {
    //console.log("test complete!");
    phantom.exit();
  }
}, 5000);