var exec = require('child_process').exec;
var child;

var http = require('http');
http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	
	child = exec("phantomjs crawler.js", function (error, stdout, stderr) {
		res.write('stdout: ' + stdout);

		if (error !== null) {
	    	console.log('exec error: ' + error);
	  	}
	  	res.end();
	});
	
}).listen(8080, '165.225.166.133');
console.log('Server running at http://165.225.166.133:8080/');