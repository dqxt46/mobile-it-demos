# Notification Demo

## Prerequisites
* cordova
* nodejs
* phantomjs

## Intro
This is a simple Phonegap/Cordova native notification demo.
The mobile app calls a nodejs app via AJAX to retrieve new product info.
The nodejs app in turn uses Phantomjs to log into and crawl Partner Central to grab the new products information.
``````
+---------------+         +------------------+            +----------------+
.               .         .                  .            .                .
.  Mobile App   .<------->.  Nodejs          .<---------->.  Phantomjs     .
.               .         .  (external.js)   .            .  (crawler.js)  .
.               .         .                  .            .                .
+---------------+         +------------------+            +----------------+
``````